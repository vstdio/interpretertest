#pragma once
#include "IEvaluationContext.h"
#include "IASTFunction.h"
#include <memory>
#include <vector>

class AST
{
public:
	AST(std::vector<std::unique_ptr<IASTFunction>> && functions);

	void Execute();

private:
	std::vector<std::unique_ptr<IASTFunction>> m_functions;
	std::unique_ptr<IEvaluationContext> m_context;
};
