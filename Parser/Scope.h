#pragma once
#include <string>
#include <optional>
#include <unordered_map>

class Scope
{
public:
	void Define(const std::string& name);
	void Assign(const std::string& name, double value);
	bool GetValue(const std::string& name, std::optional<double>& value);

private:
	std::unordered_map<std::string, std::optional<double>> m_values;
};
