﻿#include "stdafx.h"
#include "Parser.h"

namespace
{
const std::unordered_map<std::string, ASTBinaryExpression::Operator> OPERATORS_MAP = {
	{ "+", ASTBinaryExpression::Operator::Add },
	{ "-", ASTBinaryExpression::Operator::Sub },
	{ "*", ASTBinaryExpression::Operator::Mul },
	{ "/", ASTBinaryExpression::Operator::Div },
	{ "<", ASTBinaryExpression::Operator::Less },
	{ "==", ASTBinaryExpression::Operator::Equals },
	{ "!=", ASTBinaryExpression::Operator::NotEquals },
	{ "&&", ASTBinaryExpression::Operator::And },
	{ "||", ASTBinaryExpression::Operator::Or },
};
}

Parser::Parser(ILexer& lexer)
	: m_tokens()
	, m_lastMatched(nullptr)
{
	bool eof = false;
	while (!eof)
	{
		Token token = lexer.GetNextToken();
		if (token.kind != TokenKind::Whitespace &&
			token.kind != TokenKind::EndOfLine &&
			token.kind != TokenKind::Comment)
		{
			m_tokens.push_back(token);
		}
		eof = token.kind == TokenKind::EndOfFile;
	}
}

std::unique_ptr<AST> Parser::ParseAST()const
{
	std::vector<std::unique_ptr<IASTFunction>> functions;

	size_t offset = 0u;
	while (auto fn = ParseFunction(offset))
	{
		functions.push_back(std::move(fn));
	}

	if (m_tokens.at(offset).kind == TokenKind::EndOfFile)
	{
		return std::make_unique<AST>(std::move(functions));
	}
	return nullptr;
}

bool Parser::MatchStoreAndConsume(TokenKind kind, size_t& offset)const
{
	if (m_tokens.at(offset).kind == kind)
	{
		m_lastMatched = std::addressof(m_tokens.at(offset++));
		return true;
	}
	return false;
}

// Считывает все аргументы функции в массив 'args' вплоть до двоеточия
bool Parser::ParseFunctionArgumentsList(size_t& offset, std::vector<std::string>& args)const
{
	size_t copy = offset;
	if (MatchStoreAndConsume(TokenKind::Identifier, copy))
	{
		args.push_back(m_lastMatched->lexem);
		while (MatchStoreAndConsume(TokenKind::DelimiterComma, copy))
		{
			if (MatchStoreAndConsume(TokenKind::Identifier, copy))
			{
				args.push_back(m_lastMatched->lexem);
			}
			else
			{
				return false;
			}
		}
	}
	if (MatchStoreAndConsume(TokenKind::DelimiterColon, copy) ||
		MatchStoreAndConsume(TokenKind::DelimiterDotComma, copy))
	{
		offset = copy;
		return true;
	}
	return false;
}

std::unique_ptr<IASTFunction> Parser::ParseFunction(size_t& offset)const
{
	size_t copy = offset;
	if (MatchStoreAndConsume(TokenKind::Identifier, copy))
	{
		const std::string name = m_lastMatched->lexem;
		std::vector<std::string> parameters;
		if (ParseFunctionArgumentsList(copy, parameters))
		{
			auto statement = ParseStatement(copy);
			if (statement)
			{
				offset = copy;
				return std::make_unique<ASTFunction>(
					std::move(name), std::move(parameters), std::move(statement));
			}
		}
	}
	return nullptr;
}

std::unique_ptr<IASTStatement> Parser::ParseStatement(size_t& offset)const
{
	size_t copy = offset;
	if (MatchStoreAndConsume(TokenKind::IfKeyword, copy))
	{
		auto parsed = ParseConditionStatement(copy);
		if (parsed)
		{
			offset = copy;
			return parsed;
		}
	}
	else if (MatchStoreAndConsume(TokenKind::WhileKeyword, copy))
	{
		auto parsed = ParseWhileStatement(copy);
		if (parsed)
		{
			offset = copy;
			return parsed;
		}
	}
	else if (MatchStoreAndConsume(TokenKind::ReturnKeyword, copy))
	{
		auto parsed = ParseReturnStatement(copy);
		if (parsed)
		{
			offset = copy;
			return parsed;
		}
	}
	else if (MatchStoreAndConsume(TokenKind::VarKeyword, copy))
	{
		auto parsed = ParseAssignmentStatement(copy);
		if (parsed)
		{
			offset = copy;
			return parsed;
		}
		else if (MatchStoreAndConsume(TokenKind::Identifier, copy))
		{
			const std::string name = m_lastMatched->lexem;
			if (MatchStoreAndConsume(TokenKind::DelimiterDotComma, copy))
			{
				offset = copy;
				return std::make_unique<ASTAssignmentStatement>(name, nullptr);
			}
		}
	}
	else if (MatchStoreAndConsume(TokenKind::OpenCurlyBrace, copy))
	{
		auto parsed = ParseStatementSequence(copy);
		if (parsed)
		{
			offset = copy;
			return parsed;
		}
	}

	// Инструкция чтения или записи
	if (auto print = ParsePrintStatement(copy))
	{
		offset = copy;
		return print;
	}
	else if (auto scan = ParseScanStatement(copy))
	{
		offset = copy;
		return scan;
	}

	size_t backtrack = copy;
	if (MatchStoreAndConsume(TokenKind::Identifier, backtrack))
	{
		const std::string name = m_lastMatched->lexem;
		if (MatchStoreAndConsume(TokenKind::OperatorAssign, backtrack))
		{
			auto expression = ParseExpression(backtrack);
			if (expression && MatchStoreAndConsume(TokenKind::DelimiterDotComma, backtrack))
			{
				offset = backtrack;
				return std::make_unique<ASTAssignmentStatement>(name, std::move(expression));
			}
		}
	}

	auto expression = ParseExpression(copy);
	if (MatchStoreAndConsume(TokenKind::DelimiterDotComma, copy))
	{
		offset = copy;
		return std::make_unique<ASTRegularStatement>(std::move(expression));
	}
	return nullptr;
}

std::unique_ptr<ASTConditionStatement> Parser::ParseConditionStatement(size_t& offset)const
{
	size_t copy = offset;
	assert(m_lastMatched->kind == TokenKind::IfKeyword);
	if (MatchStoreAndConsume(TokenKind::OpenParenthesis, copy))
	{
		auto expression = ParseExpression(copy);
		if (expression && MatchStoreAndConsume(TokenKind::CloseParenthesis, copy))
		{
			auto then = ParseStatement(copy);
			if (then)
			{
				if (MatchStoreAndConsume(TokenKind::ElseKeyword, copy))
				{
					auto elif = ParseStatement(copy);
					if (elif)
					{
						offset = copy;
						return std::make_unique<ASTConditionStatement>(
							std::move(expression), std::move(then), std::move(elif));
					}
				}
				else
				{
					offset = copy;
					return std::make_unique<ASTConditionStatement>(
						std::move(expression), std::move(then), nullptr);
				}
			}
		}
	}
	return nullptr;
}

std::unique_ptr<ASTLoopStatement> Parser::ParseWhileStatement(size_t& offset)const
{
	size_t copy = offset;
	assert(m_lastMatched->kind == TokenKind::WhileKeyword);
	if (MatchStoreAndConsume(TokenKind::OpenParenthesis, copy))
	{
		auto expression = ParseExpression(copy);
		if (expression && MatchStoreAndConsume(TokenKind::CloseParenthesis, copy))
		{
			auto body = ParseStatement(copy);
			if (body)
			{
				offset = copy;
				return std::make_unique<ASTLoopStatement>(std::move(expression), std::move(body));
			}
		}
	}
	return nullptr;
}

std::unique_ptr<ASTReturnStatement> Parser::ParseReturnStatement(size_t& offset)const
{
	size_t copy = offset;
	assert(m_lastMatched->kind == TokenKind::ReturnKeyword);
	auto expression = ParseExpression(copy);
	if (expression && MatchStoreAndConsume(TokenKind::DelimiterDotComma, copy))
	{
		offset = copy;
		return std::make_unique<ASTReturnStatement>(std::move(expression));
	}
	else if (!expression && MatchStoreAndConsume(TokenKind::DelimiterDotComma, copy))
	{
		offset = copy;
		return std::make_unique<ASTReturnStatement>(nullptr);
	}
	return nullptr;
}

std::unique_ptr<ASTDefineVariableStatement> Parser::ParseAssignmentStatement(size_t& offset)const
{
	size_t copy = offset;
	assert(m_lastMatched->kind == TokenKind::VarKeyword);
	if (MatchStoreAndConsume(TokenKind::Identifier, copy))
	{
		const std::string name = m_lastMatched->lexem;
		if (MatchStoreAndConsume(TokenKind::OperatorAssign, copy))
		{
			auto expression = ParseExpression(copy);
			if (expression && MatchStoreAndConsume(TokenKind::DelimiterDotComma, copy))
			{
				offset = copy;
				return std::make_unique<ASTDefineVariableStatement>(name, std::move(expression));
			}
		}
	}
	return nullptr;
}

std::unique_ptr<IASTStatement> Parser::ParseStatementSequence(size_t& offset)const
{
	size_t copy = offset;
	assert(m_lastMatched->kind == TokenKind::OpenCurlyBrace);
	std::vector<std::unique_ptr<IASTStatement>> statements;
	while (!MatchStoreAndConsume(TokenKind::CloseCurlyBrace, copy))
	{
		auto parsed = ParseStatement(copy);
		if (!parsed)
		{
			return nullptr;
		}
		statements.push_back(std::move(parsed));
	}
	offset = copy;
	return std::make_unique<ASTStatementSequence>(std::move(statements));
}

std::unique_ptr<ASTPrintStatement> Parser::ParsePrintStatement(size_t& offset)const
{
	size_t copy = offset;
	if (MatchStoreAndConsume(TokenKind::OtherKeyword, copy) &&
		m_lastMatched->lexem == "printf")
	{
		if (MatchStoreAndConsume(TokenKind::OpenParenthesis, copy) &&
			MatchStoreAndConsume(TokenKind::StringLiteral, copy))
		{
			const std::string str = m_lastMatched->lexem;
			if (MatchStoreAndConsume(TokenKind::DelimiterComma, copy) && MatchStoreAndConsume(TokenKind::Identifier, copy))
			{
				const std::string variable = m_lastMatched->lexem;
				if (MatchStoreAndConsume(TokenKind::CloseParenthesis, copy) &&
					MatchStoreAndConsume(TokenKind::DelimiterDotComma, copy))
				{
					offset = copy;
					return std::make_unique<ASTPrintStatement>(std::move(str), std::make_optional<std::string>(variable));
				}
			}
			else if (MatchStoreAndConsume(TokenKind::CloseParenthesis, copy) &&
				MatchStoreAndConsume(TokenKind::DelimiterDotComma, copy))
			{
				offset = copy;
				return std::make_unique<ASTPrintStatement>(std::move(str), std::nullopt);
			}
		}
	}
	return nullptr;
}

std::unique_ptr<ASTScanStatement> Parser::ParseScanStatement(size_t& offset)const
{
	size_t copy = offset;
	if (MatchStoreAndConsume(TokenKind::OtherKeyword, copy) &&
		m_lastMatched->lexem == "scanf")
	{
		if (MatchStoreAndConsume(TokenKind::OpenParenthesis, copy) &&
			MatchStoreAndConsume(TokenKind::Identifier, copy))
		{
			const std::string name = m_lastMatched->lexem;
			if (MatchStoreAndConsume(TokenKind::CloseParenthesis, copy) &&
				MatchStoreAndConsume(TokenKind::DelimiterDotComma, copy))
			{
				offset = copy;
				return std::make_unique<ASTScanStatement>(name);
			}
		}
	}
	return nullptr;
}

std::unique_ptr<IASTExpression> Parser::ParseExpression(size_t& offset)const
{
	size_t copy = offset;
	auto expression = ParseLogicalExpression(copy);
	if (expression)
	{
		offset = copy;
		return expression;
	}
	return nullptr;
}

// Самый низкий приоритет у операторов логического сравнения
std::unique_ptr<IASTExpression> Parser::ParseLogicalExpression(size_t& offset)const
{
	size_t copy = offset;
	auto left = ParseCompareExpression(copy);
	if (!left)
	{
		return nullptr;
	}

	while (true)
	{
		if (!MatchStoreAndConsume(TokenKind::OperatorLogicalAnd, copy) &&
			!MatchStoreAndConsume(TokenKind::OperatorLogicalOr, copy))
		{
			offset = copy;
			return left;
		}

		auto op = OPERATORS_MAP.at(m_lastMatched->lexem);
		auto right = ParseCompareExpression(copy);
		if (!right)
		{
			return nullptr;
		}

		auto expression = std::make_unique<ASTBinaryExpression>(op, std::move(left), std::move(right));
		left = std::move(expression);
	}
}

std::unique_ptr<IASTExpression> Parser::ParseCompareExpression(size_t& offset)const
{
	size_t copy = offset;
	auto left = ParseSumSubExpression(copy);
	if (!left)
	{
		return nullptr;
	}

	while (true)
	{
		if (!MatchStoreAndConsume(TokenKind::OperatorCompareEqual, copy) &&
			!MatchStoreAndConsume(TokenKind::OperatorCompareLess, copy) &&
			!MatchStoreAndConsume(TokenKind::OperatorCompareNotEqual, copy))
		{
			offset = copy;
			return left;
		}

		auto op = OPERATORS_MAP.at(m_lastMatched->lexem);
		auto right = ParseSumSubExpression(copy);
		if (!right)
		{
			return nullptr;
		}

		auto expression = std::make_unique<ASTBinaryExpression>(op, std::move(left), std::move(right));
		left = std::move(expression);
	}
}

// У операторов +, - приоритет чуть выше
std::unique_ptr<IASTExpression> Parser::ParseSumSubExpression(size_t & offset) const
{
	size_t copy = offset;
	auto left = ParseMulDivExpression(copy);
	if (!left)
	{
		return nullptr;
	}

	while (true)
	{
		if (!MatchStoreAndConsume(TokenKind::OperatorPlus, copy) &&
			!MatchStoreAndConsume(TokenKind::OperatorMinus, copy))
		{
			offset = copy;
			return left;
		}

		auto op = OPERATORS_MAP.at(m_lastMatched->lexem);
		auto right = ParseMulDivExpression(copy);
		if (!right)
		{
			return nullptr;
		}

		auto expression = std::make_unique<ASTBinaryExpression>(op, std::move(left), std::move(right));
		left = std::move(expression);
	}
}

// А у операторов *, / самый высокий приоритет
std::unique_ptr<IASTExpression> Parser::ParseMulDivExpression(size_t & offset) const
{
	size_t copy = offset;
	auto left = ParseAtomicExpression(copy);
	if (!left)
	{
		return nullptr;
	}

	while (true)
	{
		if (!MatchStoreAndConsume(TokenKind::OperatorMultiplication, copy) &&
			!MatchStoreAndConsume(TokenKind::OperatorDivision, copy))
		{
			offset = copy;
			return left;
		}

		auto op = OPERATORS_MAP.at(m_lastMatched->lexem);
		auto right = ParseAtomicExpression(copy);
		if (!right)
		{
			return nullptr;
		}

		auto expression = std::make_unique<ASTBinaryExpression>(op, std::move(left), std::move(right));
		left = std::move(expression);
	}
}

// Выражения в скобках, числа, переменные и вызовы функций - это минимальные "строительные блоки" любого выражения
std::unique_ptr<IASTExpression> Parser::ParseAtomicExpression(size_t& offset)const
{
	size_t copy = offset;
	// Случай 1: может быть просто число
	if (MatchStoreAndConsume(TokenKind::OperatorMinus, copy))
	{
		if (MatchStoreAndConsume(TokenKind::Integer, copy) ||
			MatchStoreAndConsume(TokenKind::Float, copy))
		{
			offset = copy;
			return std::make_unique<ASTNumberExpression>(-std::stod(m_lastMatched->lexem));
		}
		if (MatchStoreAndConsume(TokenKind::Identifier, copy))
		{
			offset = copy;
			return std::make_unique<ASTIdentifierExpression>(m_lastMatched->lexem, true);
		}
	}
	if (MatchStoreAndConsume(TokenKind::Integer, copy) ||
		MatchStoreAndConsume(TokenKind::Float, copy))
	{
		offset = copy;
		return std::make_unique<ASTNumberExpression>(std::stod(m_lastMatched->lexem));
	}
	if (MatchStoreAndConsume(TokenKind::Identifier, copy))
	{
		const std::string name = m_lastMatched->lexem;
		// Случай 2: вызов функции
		if (MatchStoreAndConsume(TokenKind::OpenParenthesis, copy))
		{
			std::vector<std::unique_ptr<IASTExpression>> args;
			bool close = MatchStoreAndConsume(TokenKind::CloseParenthesis, copy);
			while (!close)
			{
				auto arg = ParseExpression(copy);
				if (!arg)
				{
					return nullptr;
				}
				args.push_back(std::move(arg));
				if (MatchStoreAndConsume(TokenKind::CloseParenthesis, copy))
				{
					close = true;
				}
				else if (!MatchStoreAndConsume(TokenKind::DelimiterComma, copy))
				{
					return nullptr;
				}
			}
			offset = copy;
			return std::make_unique<ASTFunctionCallExpression>(name, std::move(args));
		}
		// Случай 3: обращение к переменной
		offset = copy;
		return std::make_unique<ASTIdentifierExpression>(m_lastMatched->lexem);
	}
	if (MatchStoreAndConsume(TokenKind::OpenParenthesis, copy))
	{
		auto expression = ParseExpression(copy);
		if (expression && MatchStoreAndConsume(TokenKind::CloseParenthesis, copy))
		{
			offset = copy;
			return expression;
		}
	}
	return nullptr;
}
