#pragma once
#include <string>
#include <optional>
class IASTFunction;

class IEvaluationContext
{
public:
	virtual void Define(const std::string& name) = 0;
	virtual void Assign(const std::string& name, double value) = 0;
	virtual std::optional<double> GetValue(const std::string& name) = 0;

	virtual void PushScope(IASTFunction* function = nullptr) = 0;
	virtual void PopScope(bool isFunction) = 0;

	virtual void AddFunction(IASTFunction& function) = 0;
	virtual IASTFunction* GetFunction(const std::string& name) = 0;
	virtual IASTFunction* GetCurrentFunction() = 0;
};
