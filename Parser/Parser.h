﻿#pragma once

#include "AST.h"
#include "IASTFunction.h"
#include "IASTStatement.h"
#include "IASTExpression.h"
#include "../Lexer/ILexer.h"
#include "EvaluationContext.h"

#include <memory>

class Parser
{
public:
	Parser(ILexer& lexer);
	std::unique_ptr<AST> ParseAST()const;

private:
	bool MatchStoreAndConsume(TokenKind kind, size_t& offset)const;

	bool ParseFunctionArgumentsList(size_t& offset, std::vector<std::string>& parameters)const;
	std::unique_ptr<IASTFunction> ParseFunction(size_t& offset)const;

	std::unique_ptr<IASTStatement> ParseStatement(size_t& offset)const;
	std::unique_ptr<ASTConditionStatement> ParseConditionStatement(size_t& offset)const;
	std::unique_ptr<ASTLoopStatement> ParseWhileStatement(size_t& offset)const;
	std::unique_ptr<ASTReturnStatement> ParseReturnStatement(size_t& offset)const;
	std::unique_ptr<ASTDefineVariableStatement> ParseAssignmentStatement(size_t& offset)const;
	std::unique_ptr<IASTStatement> ParseStatementSequence(size_t& offset)const;
	std::unique_ptr<ASTPrintStatement> ParsePrintStatement(size_t& offset)const;
	std::unique_ptr<ASTScanStatement> ParseScanStatement(size_t& offset)const;

	std::unique_ptr<IASTExpression> ParseExpression(size_t& offset)const;
	std::unique_ptr<IASTExpression> ParseLogicalExpression(size_t& offset)const;
	std::unique_ptr<IASTExpression> ParseCompareExpression(size_t& offset)const;
	std::unique_ptr<IASTExpression> ParseSumSubExpression(size_t& offset)const;
	std::unique_ptr<IASTExpression> ParseMulDivExpression(size_t& offset)const;
	std::unique_ptr<IASTExpression> ParseAtomicExpression(size_t& offset)const;

	mutable const Token* m_lastMatched;
	std::vector<Token> m_tokens;
};
