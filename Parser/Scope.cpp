#include "stdafx.h"
#include "Scope.h"

void Scope::Define(const std::string& name)
{
	auto found = m_values.find(name);
	if (found != m_values.end())
	{
		found->second = std::nullopt;
	}
	m_values.emplace(name, std::nullopt);
}

void Scope::Assign(const std::string& name, double value)
{
	auto found = m_values.find(name);
	if (found == m_values.end())
	{
		throw std::runtime_error("variable " + name + " is not defined");
	}
	found->second = value;
}

bool Scope::GetValue(const std::string& name, std::optional<double>& value)
{
	auto found = m_values.find(name);
	if (found != m_values.end())
	{
		value = found->second;
		return true;
	}
	return false;
}
