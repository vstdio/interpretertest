#pragma once
#include <vector>
#include <memory>
#include <optional>

class IEvaluationContext;
class IASTStatement;

class IASTFunction
{
public:
	// ���� std::nullopt, �� ������� void, ����� ���������� ��������
	virtual std::optional<double> Evaluate(IEvaluationContext& context, std::vector<double> && args) = 0;
	virtual std::string GetName()const = 0;
	virtual bool HasReturned()const = 0;
	virtual void SetReturnValue(std::optional<double> value) = 0;
};

class ASTFunction : public IASTFunction
{
public:
	ASTFunction(const std::string& name, std::vector<std::string> && args, std::unique_ptr<IASTStatement> && statement);

	std::optional<double> Evaluate(IEvaluationContext& context, std::vector<double> && args)override;
	std::string GetName()const override;
	bool HasReturned()const override;
	void SetReturnValue(std::optional<double> value)override;

private:
	std::string m_name;
	std::vector<std::string> m_args;
	std::unique_ptr<IASTStatement> m_statement;
	bool m_hasReturned;
	std::optional<double> m_returnValue;
};
