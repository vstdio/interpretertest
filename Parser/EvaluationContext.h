#pragma once
#include "IEvaluationContext.h"
#include "Scope.h"
#include <vector>

class EvaluationContext : public IEvaluationContext
{
public:
	EvaluationContext();

	void Define(const std::string& name)override;
	void Assign(const std::string& name, double value)override;
	std::optional<double> GetValue(const std::string& name)override;

	void PushScope(IASTFunction* fn = nullptr)override;
	void PopScope(bool isFunction)override;

	void AddFunction(IASTFunction& function)override;
	IASTFunction* GetFunction(const std::string& name)override;
	IASTFunction* GetCurrentFunction()override;

private:
	std::vector<Scope> m_scopes;
	std::vector<IASTFunction*> m_functions;
	std::vector<IASTFunction*> m_callStack;
};
