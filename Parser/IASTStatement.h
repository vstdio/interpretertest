﻿#pragma once
#include "IASTExpression.h"
#include <vector>
#include <memory>

class IEvaluationContext;
class IASTExpression;

class IASTStatement
{
public:
	virtual void Execute(IEvaluationContext& context) = 0;
};

// Примеры таких инструкций:
// foo(); - просто вызов функции
// 123;   - просто выражение
// abc;   - просто переменная
class ASTRegularStatement : public IASTStatement
{
public:
	ASTRegularStatement(std::unique_ptr<IASTExpression> && expression);
	void Execute(IEvaluationContext& context)override;

private:
	std::unique_ptr<IASTExpression> m_expression;
};

class ASTDefineVariableStatement : public IASTStatement
{
public:
	ASTDefineVariableStatement(const std::string& name,
		std::unique_ptr<IASTExpression> && expression);
	void Execute(IEvaluationContext& context)override;

private:
	std::string m_name;
	std::unique_ptr<IASTExpression> m_expression;
};

class ASTAssignmentStatement : public IASTStatement
{
public:
	ASTAssignmentStatement(const std::string& name,
		std::unique_ptr<IASTExpression> && expression);
	void Execute(IEvaluationContext& context)override;

private:
	std::string m_name;
	std::unique_ptr<IASTExpression> m_expression;
};

class ASTConditionStatement : public IASTStatement
{
public:
	ASTConditionStatement(
		std::unique_ptr<IASTExpression> && expression,
		std::unique_ptr<IASTStatement> && then,
		std::unique_ptr<IASTStatement> && elif);
	void Execute(IEvaluationContext& context)override;

private:
	std::unique_ptr<IASTExpression> m_expression;
	std::unique_ptr<IASTStatement> m_then;
	std::unique_ptr<IASTStatement> m_elif;
};

class ASTLoopStatement : public IASTStatement
{
public:
	ASTLoopStatement(
		std::unique_ptr<IASTExpression> && expression,
		std::unique_ptr<IASTStatement> && body);
	void Execute(IEvaluationContext& context)override;

private:
	std::unique_ptr<IASTExpression> m_expression;
	std::unique_ptr<IASTStatement> m_body;
};

class ASTReturnStatement : public IASTStatement
{
public:
	ASTReturnStatement(std::unique_ptr<IASTExpression> && expression);
	void Execute(IEvaluationContext& context)override;

private:
	std::unique_ptr<IASTExpression> m_expression;
};

class ASTStatementSequence : public IASTStatement
{
public:
	ASTStatementSequence(std::vector<std::unique_ptr<IASTStatement>> && sequence);
	void Execute(IEvaluationContext& context)override;

private:
	std::vector<std::unique_ptr<IASTStatement>> m_sequence;
};

class ASTPrintStatement : public IASTStatement
{
public:
	ASTPrintStatement(const std::string& str, std::optional<std::string> variable);
	void Execute(IEvaluationContext& context)override;

private:
	std::string m_str;
	std::optional<std::string> m_variable;
};

class ASTScanStatement : public IASTStatement
{
public:
	ASTScanStatement(const std::string& name);
	void Execute(IEvaluationContext& context)override;

private:
	std::string m_str;
	std::string m_name;
};
