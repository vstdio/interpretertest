#include "stdafx.h"
#include "AST.h"
#include "EvaluationContext.h"

AST::AST(std::vector<std::unique_ptr<IASTFunction>> && functions)
	: m_functions(std::move(functions))
	, m_context(std::make_unique<EvaluationContext>())
{
}

void AST::Execute()
{
	for (auto& function : m_functions)
	{
		assert(function.get());
		m_context->AddFunction(*function);
	}

	auto main = m_context->GetFunction("main");
	if (!main)
	{
		throw std::runtime_error("main function not found");
	}

	auto result = main->Evaluate(*m_context, std::vector<double>());
}
