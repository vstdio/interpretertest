#pragma once
#include "IEvaluationContext.h"
#include <memory>
#include <vector>
#include <string>

class IASTExpression
{
public:
	virtual double Evaluate(IEvaluationContext& context) = 0;
};

class ASTNumberExpression : public IASTExpression
{
public:
	ASTNumberExpression(double value, bool negative = false);
	double Evaluate(IEvaluationContext& context)override;

private:
	bool m_negative;
	double m_value;
};

class ASTIdentifierExpression : public IASTExpression
{
public:
	ASTIdentifierExpression(const std::string& name, bool negative = false);
	double Evaluate(IEvaluationContext& context)override;

private:
	bool m_negative;
	std::string m_name;
};

class ASTFunctionCallExpression : public IASTExpression
{
public:
	ASTFunctionCallExpression(const std::string& name,
		std::vector<std::unique_ptr<IASTExpression>> && args);
	double Evaluate(IEvaluationContext& context)override;

private:
	std::string m_fnName;
	std::vector<std::unique_ptr<IASTExpression>> m_args;
};

class ASTBinaryExpression : public IASTExpression
{
public:
	enum class Operator
	{
		Add,
		Sub,
		Mul,
		Div,
		Less,
		Equals,
		NotEquals,
		And,
		Or,
		Nop
	};

	ASTBinaryExpression(Operator op,
		std::unique_ptr<IASTExpression> && left,
		std::unique_ptr<IASTExpression> && right);
	double Evaluate(IEvaluationContext& context)override;

private:
	Operator m_op;
	std::unique_ptr<IASTExpression> m_left;
	std::unique_ptr<IASTExpression> m_right;
};
