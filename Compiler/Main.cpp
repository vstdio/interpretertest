#include "stdafx.h"
#include "../Lexer/Lexer.h"
#include "../Parser/Parser.h"

namespace
{
std::string GetFileContent(const std::string& filePath)
{
	std::ifstream input(filePath);
	assert(input); // TODO: throw exception here
	std::stringstream buffer;
	buffer << input.rdbuf();
	return buffer.str();
}

void PrintUsageInstructions(std::ostream& strm)
{
	strm << "Usage: Compiler.exe <file path>" << std::endl;
}
}

int main(int argc, char* argv[])
{
	if (argc != 2)
	{
		std::cerr << "Invalid arguments count!" << std::endl;
		PrintUsageInstructions(std::cerr);
		return 1;
	}

	try
	{
		Lexer lexer(GetFileContent(argv[1]));
		auto parser = std::make_unique<Parser>(lexer);
		auto ast = parser->ParseAST();
		if (ast)
		{
			std::cout << "AST successfully created! Trying to execute program..." << std::endl;
			ast->Execute();
		}
		else
		{
			std::cout << "AST building error..." << std::endl;
		}
	}
	catch (const std::exception& ex)
	{
		std::cerr << ex.what() << std::endl;
		return 1;
	}
	return 0;
}
