#pragma once
#include "../Lexer/ILexer.h"
#include <vector>

class SyntaxAnalyzer
{
public:
	SyntaxAnalyzer(const std::vector<Token> && tokens);

	bool ParseProgram();

	bool ParseFunctions(size_t& index);
	bool ParseFunction(size_t& index);

	bool ParseParamDecls(size_t& index);
	bool ParseParamDecl(size_t& index);

	bool ParseStatement(size_t& index);
	bool ParseCompoundStatement(size_t& index);

	bool ParseVarDeclStatement(size_t& index);
	bool ParseVarDeclList(size_t& index);
	bool ParseVarDecl(size_t& index);

	bool ParseExpression(size_t& index);
	bool ParseAtomicExpression(size_t& index);

private:
	bool TryParseVarDeclStatement(size_t& index);
	bool TryParseExpressionStatement(size_t& index);

	bool ContinueParseCompoundStatement(size_t& index);
	bool ContinueParseIfStatement(size_t& index);
	bool ContinueParseWhileStatement(size_t& index);
	bool ContinueParseReturnStatement(size_t& index);
	bool ContinueParseEmptyStatement(size_t& index);

	std::vector<Token> m_tokens;
};
