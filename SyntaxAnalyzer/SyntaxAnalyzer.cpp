﻿#include "stdafx.h"
#include "SyntaxAnalyzer.h"

SyntaxAnalyzer::SyntaxAnalyzer(const std::vector<Token> && tokens)
	: m_tokens(tokens)
{
}

// <Program> -> <Functions>
bool SyntaxAnalyzer::ParseProgram()
{
	size_t index = 0;
	return ParseFunctions(index) &&
		m_tokens.at(index).kind == TokenKind::EndOfFile;
}

// <Functions> -> <Function> <Functions> | <Function>
bool SyntaxAnalyzer::ParseFunctions(size_t& index)
{
	size_t copy = index;
	if (ParseFunction(copy))
	{
		ParseFunctions(copy);
		index = copy;
		return true;
	}
	return false;
}

// <Function> -> <Identifier> <ParamDecls> ':' <Stmt>
bool SyntaxAnalyzer::ParseFunction(size_t& index)
{
	size_t copy = index;
	if (m_tokens.at(copy++).kind == TokenKind::Identifier &&
		ParseParamDecls(copy) &&
		m_tokens.at(copy++).kind == TokenKind::DelimiterColon &&
		ParseStatement(copy))
	{
		index = copy;
		return true;
	}
	return false;
}

// <ParamDecls> -> <ParamDecl> | <Empty>
bool SyntaxAnalyzer::ParseParamDecls(size_t& index)
{
	size_t copy = index;
	if (ParseParamDecl(copy))
	{
		index = copy;
		return true;
	}
	return m_tokens.at(copy).kind == TokenKind::DelimiterColon;
}

// <ParamDecl> -> <Identifier> ',' <ParamDecl> | <Identifier>
bool SyntaxAnalyzer::ParseParamDecl(size_t& index)
{
	size_t copy = index;
	if (m_tokens.at(copy++).kind == TokenKind::Identifier)
	{
		if (m_tokens.at(copy).kind == TokenKind::DelimiterComma)
		{
			++copy;
			if (ParseParamDecl(copy))
			{
				index = copy;
				return true;
			}
		}
		else if (m_tokens.at(copy).kind == TokenKind::DelimiterColon)
		{
			index = copy;
			return true;
		}
	}
	return false;
}

// <Statement> -> '{' <CompoundStatement> '}'
// |              'if' '(' <Expression> ')' <Statement>
// |              'if' '(' <Expression> ')' <Statement> 'else' <Statement>
// |              'while' '(' <Expression> ')' <Statement>
// |              'return' <Expression> ';'
// |              <VarDefs> ';'
// |              <Expression> ';'
// |              ';'
bool SyntaxAnalyzer::ParseStatement(size_t& index)
{
	size_t copy = index;

	using DispatchTable = std::unordered_map<std::string, std::function<bool(size_t&)>>;
	static const DispatchTable dispatchTable = {
		{ "if", std::bind(&SyntaxAnalyzer::ContinueParseIfStatement, this, std::placeholders::_1) },
		{ "while", std::bind(&SyntaxAnalyzer::ContinueParseWhileStatement, this, std::placeholders::_1) },
		{ "return", std::bind(&SyntaxAnalyzer::ContinueParseReturnStatement, this, std::placeholders::_1) },
		{ ";", std::bind(&SyntaxAnalyzer::ContinueParseEmptyStatement, this, std::placeholders::_1) },
		{ "{", std::bind(&SyntaxAnalyzer::ContinueParseCompoundStatement, this, std::placeholders::_1) }
	};

	auto found = dispatchTable.find(m_tokens.at(copy).lexem);
	if (found != dispatchTable.end())
	{
		if (found->second(++copy))
		{
			index = copy;
			return true;
		}
		return false;
	}

	if (TryParseVarDeclStatement(copy))
	{
		index = copy;
		return true;
	}
	if (TryParseExpressionStatement(copy))
	{
		index = copy;
		return true;
	}
	return false;
}

// <CompoundStatement> -> <Statement> <CompoundStatement> | <Empty>
bool SyntaxAnalyzer::ParseCompoundStatement(size_t & index)
{
	size_t copy = index;
	if (ParseStatement(copy))
	{
		ParseCompoundStatement(copy); // It can be empty, so we ignore the result
		index = copy;
		return true;
	}
	// Returns true on empty rule production
	return m_tokens.at(copy).kind == TokenKind::CloseCurlyBrace;
}

// <VarDeclStatement> -> 'var' <VarDeclList>
bool SyntaxAnalyzer::ParseVarDeclStatement(size_t& index)
{
	size_t copy = index;
	if (m_tokens.at(copy++).lexem == "var" &&
		ParseVarDeclList(copy))
	{
		index = copy;
		return true;
	}
	return false;
}

// <VarDeclList> -> <VarDecl> ',' <VarDeclList> | <VarDecl>
bool SyntaxAnalyzer::ParseVarDeclList(size_t& index)
{
	size_t copy = index;
	if (ParseVarDecl(copy))
	{
		if (m_tokens.at(copy).kind == TokenKind::DelimiterComma)
		{
			if (ParseVarDeclList(++copy))
			{
				index = copy;
				return true;
			}
			return false;
		}
		index = copy;
		return true;
	}
	return false;
}

// <VarDecl> -> <Identifier> '=' <Expression> | <Identifier>
bool SyntaxAnalyzer::ParseVarDecl(size_t& index)
{
	size_t copy = index;
	if (m_tokens.at(copy++).kind == TokenKind::Identifier)
	{
		if (m_tokens.at(copy).kind == TokenKind::OperatorAssign)
		{
			if (ParseExpression(++copy))
			{
				index = copy;
				return true;
			}
			return false;
		}
		index = copy;
		return true;
	}
	return false;
}

bool SyntaxAnalyzer::ParseExpression(size_t& index)
{
	size_t copy = index;
	Token& token = m_tokens.at(copy);
	// '*' <Expression>
	// '-' <Expression>
	// '!' <Expression>
	if (token.kind == TokenKind::OperatorMultiplication ||
		token.kind == TokenKind::OperatorMinus ||
		token.kind == TokenKind::OperatorLogicalNegation)
	{
		if (ParseExpression(++copy))
		{
			index = copy;
			return true;
		}
		return false;
	}
	if (ParseAtomicExpression(copy))
	{
		size_t backtrack = copy;
		if (m_tokens.at(copy++).kind == TokenKind::OpenSquareBracket &&
			ParseExpression(copy) &&
			m_tokens.at(copy++).kind == TokenKind::CloseSquareBracket)
		{
			index = copy;
			return true;
		}
		copy = backtrack;
		if (m_tokens.at(copy++).kind == TokenKind::OpenParenthesis &&
			m_tokens.at(copy++).kind == TokenKind::CloseParenthesis)
		{
			index = copy;
			return true;
		}
		copy = backtrack;
		if (m_tokens.at(copy).kind == TokenKind::OperatorAssign ||
			m_tokens.at(copy).kind == TokenKind::OperatorPlus ||
			m_tokens.at(copy).kind == TokenKind::OperatorMinus ||
			m_tokens.at(copy).kind == TokenKind::OperatorMultiplication ||
			m_tokens.at(copy).kind == TokenKind::OperatorDivision ||
			m_tokens.at(copy).kind == TokenKind::OperatorLogicalOr ||
			m_tokens.at(copy).kind == TokenKind::OperatorLogicalAnd ||
			m_tokens.at(copy).kind == TokenKind::OperatorCompareEqual ||
			m_tokens.at(copy).kind == TokenKind::OperatorCompareNotEqual ||
			m_tokens.at(copy).kind == TokenKind::OperatorCompareLess ||
			m_tokens.at(copy).kind == TokenKind::OperatorCompareMore ||
			m_tokens.at(copy).kind == TokenKind::OperatorCompareLessEqual ||
			m_tokens.at(copy).kind == TokenKind::OperatorCompareMoreEqual)
		{
			if (ParseExpression(++copy))
			{
				index = copy;
				return true;
			}
			return false;
		}
		index = backtrack;
		return true;
	}
	return false;
}

bool SyntaxAnalyzer::ParseAtomicExpression(size_t& index)
{
	size_t copy = index;
	// '(' <Expression> ')'
	if (m_tokens.at(copy).kind == TokenKind::OpenParenthesis)
	{
		if (ParseExpression(++copy) &&
			m_tokens.at(copy++).kind == TokenKind::CloseParenthesis)
		{
			index = copy;
			return true;
		}
		return false;
	}
	if (m_tokens.at(copy).kind == TokenKind::Identifier)
	{
		if (m_tokens.at(++copy).kind == TokenKind::OpenParenthesis)
		{
			if (ParseExpression(++copy) &&
				m_tokens.at(copy++).kind == TokenKind::CloseParenthesis)
			{
				index = copy;
				return true;
			}
		}
	}
	copy = index;
	switch (m_tokens.at(copy++).kind)
	{
	case TokenKind::Identifier:
	case TokenKind::Integer:
	case TokenKind::Float:
	case TokenKind::StringLiteral:
		index = copy;
		return true;
	default:
		return false;
	}
}

bool SyntaxAnalyzer::TryParseVarDeclStatement(size_t & index)
{
	size_t copy = index;
	if (ParseVarDeclStatement(copy) &&
		m_tokens.at(copy++).kind == TokenKind::DelimiterDotComma)
	{
		index = copy;
		return true;
	}
	return false;
}

bool SyntaxAnalyzer::TryParseExpressionStatement(size_t & index)
{
	size_t copy = index;
	if (ParseExpression(copy) &&
		m_tokens.at(copy++).kind == TokenKind::DelimiterDotComma)
	{
		index = copy;
		return true;
	}
	return false;
}

bool SyntaxAnalyzer::ContinueParseCompoundStatement(size_t& index)
{
	size_t copy = index;
	if (ParseCompoundStatement(copy) &&
		m_tokens.at(copy++).kind == TokenKind::CloseCurlyBrace)
	{
		index = copy;
		return true;
	}
	return false;
}

bool SyntaxAnalyzer::ContinueParseIfStatement(size_t & index)
{
	size_t copy = index;
	if (m_tokens.at(copy++).kind == TokenKind::OpenParenthesis &&
		ParseExpression(copy) &&
		m_tokens.at(copy++).kind == TokenKind::CloseParenthesis &&
		ParseStatement(copy))
	{
		if (m_tokens.at(copy).lexem == "else")
		{
			if (ParseStatement(++copy))
			{
				index = copy;
				return true;
			}
			return false;
		}
		index = copy;
		return true;
	}
	return false;
}

// <WhileStatement> -> 'while' '(' <Expression> ')' <Statement>
bool SyntaxAnalyzer::ContinueParseWhileStatement(size_t& index)
{
	size_t copy = index;
	if (m_tokens.at(copy++).kind == TokenKind::OpenParenthesis &&
		ParseExpression(copy) &&
		m_tokens.at(copy++).kind == TokenKind::CloseParenthesis &&
		ParseStatement(copy))
	{
		index = copy;
		return true;
	}
	return false;
}

// <ReturnStatement> -> 'return' <Expression> ';'
bool SyntaxAnalyzer::ContinueParseReturnStatement(size_t& index)
{
	size_t copy = index;
	if (ParseExpression(copy) &&
		m_tokens.at(copy++).kind == TokenKind::DelimiterDotComma)
	{
		index = copy;
		return true;
	}
	return false;
}

// <EmptyStatement> -> ';'
bool SyntaxAnalyzer::ContinueParseEmptyStatement(size_t& index)
{
	(void)index;
	return true;
}
