#pragma once
#include "TokenKind.h"

// ��������� ����� - ������� value-���
//  ������ ������, ��� ������, ������� ������,
//  ����� ������ � ������� � ������ ��������� ����
struct Token
{
	Token(const std::string& lexem,
		TokenKind kind,
		size_t index,
		size_t row,
		size_t column);

	std::string lexem;
	TokenKind kind;
	size_t index;
	size_t row;
	size_t column;
};
