﻿#pragma once
#include <string>

enum class TokenKind
{
	// Ключевые слова, идентификаторы
	IfKeyword,
	ElseKeyword,
	WhileKeyword,
	ReturnKeyword,
	VarKeyword,
	OtherKeyword,
	Identifier,

	// Базовые типы данных: целые числа, дробные числа, строковые литералы
	Integer,
	Float,
	StringLiteral,

	// Арифметические операторы языка
	OperatorPlus,
	OperatorMinus,
	OperatorMultiplication,
	OperatorDivision,

	// Управляющие операторы
	OperatorAssign,
	OperatorArrow,
	OperatorDot,
	
	// Логические операторы
	OperatorLogicalAnd,
	OperatorLogicalOr,
	OperatorLogicalNegation,

	// Операторы сравнения
	OperatorCompareEqual,
	OperatorCompareNotEqual,
	OperatorCompareLess,
	OperatorCompareMore,
	OperatorCompareLessEqual,
	OperatorCompareMoreEqual,

	// Разделители
	DelimiterComma,
	DelimiterDotComma,
	DelimiterColon,
	DelimiterDoubleColon,

	// Синтаксические конструкции языка
	OpenParenthesis,
	CloseParenthesis,
	OpenCurlyBrace,
	CloseCurlyBrace,
	OpenSquareBracket,
	CloseSquareBracket,

	// Вспомогательные токены
	Comment,
	Whitespace,
	EndOfLine,
	EndOfFile,

	// Если тип токена определить не удалось
	Undefined
};

std::string ToString(TokenKind kind);
