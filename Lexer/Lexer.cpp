﻿#include "stdafx.h"
#include "Lexer.h"
#include <unordered_set>

using namespace std::string_literals;

namespace
{
const std::regex WHITESPACE_PATTERN = std::regex("[ \t]+");
const std::regex NEWLINE_PATTERN = std::regex("\r\n|\r|\n");
const std::regex IDENTIFIER_PATTERN = std::regex("[a-zA-Z_][a-zA-Z_0-9]*");
const std::regex INTEGER_PATTERN = std::regex("0|[1-9][0-9]*");

// TODO: exponent part must have preceding digit, but "2.e1" will match
const std::regex FLOAT_PATTERN = std::regex("([0-9]+\\.[0-9]*|\\.[0-9]+)([eE][+-]?[0-9]+)?");
const std::regex STRING_LITERAL_PATTERN = std::regex("\"([^\\\\\"\n]|\\\\.)*\"");
const std::regex COMMENT_PATTERN = std::regex("\\/\\*([^*]|\\*+[^\\/*])*\\*+\\/");

const std::regex OPERATOR_PATTERN = std::regex(
	"=="
	"|!="
	"|<="
	"|>="
	"|->"
	"|&&"
	"|\\|\\|"
	"|<"
	"|>"
	"|\\+"
	"|-"
	"|\\*"
	"|/"
	"|="
	"|!"
	"|\\."
);
const std::unordered_map<std::string, TokenKind> OPERATOR_TOKEN_KIND_MAP = {
	{ "+", TokenKind::OperatorPlus },
	{ "-", TokenKind::OperatorMinus },
	{ "*", TokenKind::OperatorMultiplication },
	{ "/", TokenKind::OperatorDivision },
	{ "=", TokenKind::OperatorAssign },
	{ "->", TokenKind::OperatorArrow },
	{ "&&", TokenKind::OperatorLogicalAnd },
	{ "||", TokenKind::OperatorLogicalOr },
	{ "!", TokenKind::OperatorLogicalNegation },
	{ ".", TokenKind::OperatorDot },
	{ "==", TokenKind::OperatorCompareEqual },
	{ "!=", TokenKind::OperatorCompareNotEqual },
	{ "<=", TokenKind::OperatorCompareLessEqual },
	{ ">=", TokenKind::OperatorCompareMoreEqual },
	{ "<", TokenKind::OperatorCompareLess },
	{ ">", TokenKind::OperatorCompareMore }
};

const std::regex DELIMITER_PATTERN = std::regex(
	","
	"|;"
	"|:"
	"|::"
);
const std::unordered_map<std::string, TokenKind> DELIMITER_TOKEN_KIND_MAP = {
	{ ",", TokenKind::DelimiterComma },
	{ ";", TokenKind::DelimiterDotComma },
	{ "::", TokenKind::DelimiterDoubleColon },
	{ ":", TokenKind::DelimiterColon }
};

const std::regex BRACE_PATTERN = std::regex("\\(|\\)|\\{|\\}|\\[|\\]");
const std::unordered_map<std::string, TokenKind> BRACE_TOKEN_KIND_MAP = {
	{ "(", TokenKind::OpenParenthesis },
	{ ")", TokenKind::CloseParenthesis },
	{ "{", TokenKind::OpenCurlyBrace },
	{ "}", TokenKind::CloseCurlyBrace },
	{ "[", TokenKind::OpenSquareBracket },
	{ "]", TokenKind::CloseSquareBracket }
};

const std::unordered_set<std::string> KEYWORDS = {
	"void", "int", "float", "return", "const",
	"true", "false", "nullptr", "if", "else",
	"switch", "case", "break", "for", "while", "var",
	"printf", "scanf"
};
bool IsKeyword(const std::string& lexem)
{
	return KEYWORDS.find(lexem) != KEYWORDS.end();
}
TokenKind DetermineKeywordTokenKind(const std::string& lexem)
{
	assert(IsKeyword(lexem));
	static const std::unordered_map<std::string, TokenKind> keywordsMap = {
		{ "if", TokenKind::IfKeyword },
		{ "else", TokenKind::ElseKeyword },
		{ "while", TokenKind::WhileKeyword },
		{ "return", TokenKind::ReturnKeyword },
		{ "var", TokenKind::VarKeyword }
	};
	auto found = keywordsMap.find(lexem);
	if (found != keywordsMap.end())
	{
		return found->second;
	}
	return TokenKind::OtherKeyword;
}

// Попытается найти совпадение в тексте 'text', которое начинается с позиции текста 'offset'
//  и соответствует регулярному выражению 'regex'
bool FindMatch(const std::string& text, size_t offset, const std::regex& regex, /* out */ std::string& match)
{
	assert(offset < text.length());
	std::smatch matchResults;
	if (std::regex_search(text.begin() + offset, text.end(), matchResults, regex, std::regex_constants::match_continuous))
	{
		match = std::move(matchResults.str());
		return true;
	}
	return false;
}

struct NewlineInfo
{
	unsigned count = 0;
	size_t lastPos = std::string::npos;
};

NewlineInfo GetNewlineInfo(const std::string& text)
{
	NewlineInfo info;
	bool foundNewline = false;
	for (auto it = text.rbegin(); it != text.rend(); ++it)
	{
		if (*it == '\r\n' || *it == '\r' || *it == '\n')
		{
			if (!foundNewline)
			{
				size_t rpos = std::distance(text.rbegin(), it);
				info.lastPos = text.length() - 1 - rpos;
			}
			++info.count;
			foundNewline = true;
		}
	}
	return info;
}

template <typename StringType>
StringType Replace(const StringType& str, const StringType& search, const StringType& replace)
{
	if (search.empty())
	{
		return str;
	}

	StringType modified;
	modified.reserve(str.length());

	std::size_t offset = 0u;
	std::size_t found = str.find(search);

	while (found != StringType::npos)
	{
		modified.append(str.substr(offset, found - offset));
		modified.append(replace);
		offset = found + search.length();
		found = str.find(search, offset);
	}

	modified.append(str.substr(offset));
	return modified;
}
}

Lexer::Lexer(const std::string& text)
	: m_text(text)
	, m_index(0)
	, m_row(0)
	, m_column(0)
	, m_parsingPatterns({
		{ WHITESPACE_PATTERN, std::bind(&Lexer::OnWhitespacesMatch, this) },
		{ NEWLINE_PATTERN, std::bind(&Lexer::OnNewlineMatch, this) },
		{ COMMENT_PATTERN, std::bind(&Lexer::OnCommentMatch, this) },
		{ IDENTIFIER_PATTERN, std::bind(&Lexer::OnIdentifierMatch, this) },
		{ FLOAT_PATTERN, std::bind(&Lexer::OnFloatMatch, this) },
		{ INTEGER_PATTERN, std::bind(&Lexer::OnIntegerMatch, this) },
		{ OPERATOR_PATTERN, std::bind(&Lexer::OnOperatorMatch, this) },
		{ DELIMITER_PATTERN, std::bind(&Lexer::OnDelimiterMatch, this) },
		{ BRACE_PATTERN, std::bind(&Lexer::OnBraceMatch, this) },
		{ STRING_LITERAL_PATTERN, std::bind(&Lexer::OnStringMatch, this) }
	})
{
}

Token Lexer::GetNextToken()
{
	if (m_index >= m_text.length())
	{
		return { "", TokenKind::EndOfFile, m_index, m_row, m_column };
	}

	for (const auto& parsingInfo : m_parsingPatterns)
	{
		if (FindMatch(m_text, m_index, parsingInfo.regex, m_lastMatchedLexem))
		{
			return parsingInfo.action();
		}
	}

	return OnUndefinedMatch();
}

const std::string& Lexer::GetLastMatchedLexem() const
{
	return m_lastMatchedLexem;
}

Token Lexer::OnWhitespacesMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	Token token = { lexem, TokenKind::Whitespace, m_index, m_row, m_column };
	m_index += lexem.length();
	m_column += lexem.length();
	return token;
}

Token Lexer::OnNewlineMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	Token token = { "", TokenKind::EndOfLine, m_index, m_row, m_column };
	m_index += lexem.length();
	m_column = 0;
	++m_row;
	return token;
}

Token Lexer::OnCommentMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	Token token = { lexem, TokenKind::Comment, m_index, m_row, m_column };

	NewlineInfo newlineInfo = GetNewlineInfo(lexem);
	if (newlineInfo.count == 0)
	{
		m_column += lexem.length();
	}
	else
	{
		m_row += newlineInfo.count;
		m_column = lexem.length() - newlineInfo.lastPos - 1;
	}

	m_index += lexem.length();
	return token;
}

Token Lexer::OnIdentifierMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	TokenKind kind = IsKeyword(lexem) ? DetermineKeywordTokenKind(lexem) : TokenKind::Identifier;
	Token token = { lexem, kind, m_index, m_row, m_column };
	m_index += lexem.length();
	m_column += lexem.length();
	return token;
}

Token Lexer::OnFloatMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	Token token = { lexem, TokenKind::Float, m_index, m_row, m_column };
	m_index += lexem.length();
	m_column += lexem.length();
	return token;
}

Token Lexer::OnIntegerMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	Token token = { lexem, TokenKind::Integer, m_index, m_row, m_column };
	m_index += lexem.length();
	m_column += lexem.length();
	return token;
}

Token Lexer::OnOperatorMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	Token token = { lexem, OPERATOR_TOKEN_KIND_MAP.at(lexem), m_index, m_row, m_column };
	m_index += lexem.length();
	m_column += lexem.length();
	return token;
}

Token Lexer::OnDelimiterMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	Token token = { lexem, DELIMITER_TOKEN_KIND_MAP.at(lexem), m_index, m_row, m_column };
	m_index += lexem.length();
	m_column += lexem.length();
	return token;
}

Token Lexer::OnBraceMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	Token token = { lexem, BRACE_TOKEN_KIND_MAP.at(lexem), m_index, m_row, m_column };
	m_index += lexem.length();
	m_column += lexem.length();
	return token;
}

Token Lexer::OnStringMatch()
{
	const auto& lexem = GetLastMatchedLexem();
	assert(lexem.length() > 2u);

	// TODO: fix this
	auto unescaped = lexem.substr(1).substr(0, lexem.length() - 2u);
	unescaped = Replace(unescaped, "\\n"s, "\n"s);
	unescaped = Replace(unescaped, "\\t"s, "\t"s);

	Token token = { unescaped, TokenKind::StringLiteral, m_index, m_row, m_column };
	m_index += lexem.length();
	m_column += lexem.length();
	return token;
}

Token Lexer::OnUndefinedMatch()
{
	// Будет считан лишь один символ
	Token token = { std::string(1, m_text.at(m_index)), TokenKind::Undefined, m_index, m_row, m_column };
	m_index += 1;
	m_column += 1;
	return token;
}
