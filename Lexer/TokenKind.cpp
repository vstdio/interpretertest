#include "stdafx.h"
#include "TokenKind.h"

using namespace std::literals::string_literals;

namespace
{
std::unordered_map<TokenKind, std::string> TOKEN_KIND_STRING_MAP = {
	{ TokenKind::IfKeyword, "IfKeyword"s },
	{ TokenKind::ElseKeyword, "ElseKeyword"s },
	{ TokenKind::WhileKeyword, "WhileKeyword"s },
	{ TokenKind::ReturnKeyword, "ReturnKeyword"s },
	{ TokenKind::VarKeyword, "VarKeyword"s },
	{ TokenKind::OtherKeyword, "ReturnKeyword"s },
	{ TokenKind::Identifier, "Identifier"s },

	{ TokenKind::Integer, "Integer"s },
	{ TokenKind::Float, "Float"s },

	{ TokenKind::StringLiteral, "StringLiteral"s },

	{ TokenKind::OperatorPlus, "OperatorPlus"s },
	{ TokenKind::OperatorMinus, "OperatorMinus"s },
	{ TokenKind::OperatorMultiplication, "OperatorMultiplication"s },
	{ TokenKind::OperatorDivision, "OperatorDivision"s },
	{ TokenKind::OperatorAssign, "OperatorAssign"s },
	{ TokenKind::OperatorArrow, "OperatorArrow"s },
	{ TokenKind::OperatorLogicalAnd, "OperatorLogicalAnd"s },
	{ TokenKind::OperatorLogicalOr, "OperatorLogicalOr"s },
	{ TokenKind::OperatorLogicalNegation, "OperatorLogicalNegation"s },
	{ TokenKind::OperatorCompareEqual, "OperatorCompareEqual"s },
	{ TokenKind::OperatorCompareNotEqual, "OperatorCompareNotEqual"s },
	{ TokenKind::OperatorCompareLessEqual, "OperatorCompareLessEqual"s },
	{ TokenKind::OperatorCompareMoreEqual, "OperatorCompareMoreEqual"s},
	{ TokenKind::OperatorCompareLess, "OperatorCompareLess"s },
	{ TokenKind::OperatorCompareMore, "OperatorCompareMore"s },

	{ TokenKind::DelimiterComma, "DelimiterComma"s },
	{ TokenKind::DelimiterDotComma, "DelimiterDotComma"s },
	{ TokenKind::DelimiterColon, "DelimiterColon"s },
	{ TokenKind::DelimiterDoubleColon, "DelimiterDoubleColon"s },

	{ TokenKind::OpenParenthesis, "OpenParenthesis"s },
	{ TokenKind::CloseParenthesis, "CloseParenthesis"s },
	{ TokenKind::OpenCurlyBrace, "OpenCurlyBrace"s },
	{ TokenKind::CloseCurlyBrace, "CloseCurlyBrace"s },
	{ TokenKind::OpenSquareBracket, "OpenSquareBracket"s },
	{ TokenKind::CloseSquareBracket, "CloseSquareBracket"s },

	{ TokenKind::Comment, "Comment"s },
	{ TokenKind::Undefined, "Undefined"s },
	{ TokenKind::Whitespace, "Whitespace"s },
	{ TokenKind::EndOfLine, "EndOfLine"s },
	{ TokenKind::EndOfFile, "EndOfFile"s }
};
}

std::string ToString(TokenKind kind)
{
	auto it = TOKEN_KIND_STRING_MAP.find(kind);
	assert(it != TOKEN_KIND_STRING_MAP.end());
	return it->second;
}
