#pragma once
#include "ILexer.h"

#include <regex>
#include <unordered_map>
#include <functional>

class Lexer : public ILexer
{
public:
	Lexer(const std::string& text);
	Token GetNextToken() override;
	const std::string& GetLastMatchedLexem()const;

private:
	Token OnWhitespacesMatch();
	Token OnNewlineMatch();
	Token OnCommentMatch();
	Token OnIdentifierMatch();
	Token OnFloatMatch();
	Token OnIntegerMatch();
	Token OnOperatorMatch();
	Token OnDelimiterMatch();
	Token OnBraceMatch();
	Token OnStringMatch();
	Token OnUndefinedMatch();

	struct TokenParsingInfo
	{
		std::regex regex;
		std::function<Token()> action;
	};

	std::vector<TokenParsingInfo> m_parsingPatterns;

	std::string m_text;
	std::string m_lastMatchedLexem;

	size_t m_index;
	size_t m_row;
	size_t m_column;
};
